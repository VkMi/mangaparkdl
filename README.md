# MangaParkDL
## Info
WebCrawler to mass download mangas form the MangaPark.me website
## Usage
Paste a single url after the program in a command line or call it without parameters for more functionalities

If called without parameters you can later write:

| Parameter | Effect |
|---|---|
| exit | exit the program |
| url |  Download the linked manga chapter. Be careful to pass the url of the whole chapter and not of the page. | 
| multiple urls separated by ^ | Downloads multiple chapters. | 
| url to the first chapter -to # | Download multiple chapters of the same manga, where # is the number of the last chapter you want to download. Be carful that all chapters belong to the same volume as seen from the '/v#/' string. | 

## Examples
| Parameter | Example |
|---|---|
| [link] | http://mangapark.me/manga/blame/s1/v9/c54 | 
| [multiple links] | http://mangapark.me/manga/blame/s1/v9/c54^http://mangapark.me/manga/biomega/s1/v5/c31 |
| [multiple chapters] | http://mangapark.me/manga/abara/s1/v1/c1 -to 6 |