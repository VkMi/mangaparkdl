﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Generic_parser;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace MangaParkDL
{
    class Program
    {
        static string dl_folder_path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\mangadl";
        static int n_pages = 0;
        static string path_to_first_file_dl = "";
        static bool isPTFFDmodifiable = true;

        static void Main(string[] args)
        {
            if (!Directory.Exists(dl_folder_path)) Directory.CreateDirectory(dl_folder_path);
            if (args.Length != 1)
            {
                Console.WriteLine("Paste link to full chapter to download");
                Console.WriteLine("Or paste a bunch of links to download multiple chapters");
                Console.WriteLine("Or paste the link of the first chapter you want to download and add ' -to #' where # is the number of the last chapter");

                string chapter_url = Console.ReadLine();
                downloadFromString(chapter_url);
            }
            else if ((args[0] == "-h") || (args[0] == "--help"))
                Console.WriteLine("This program downloads whole manga chapters from mangapark.me \n\nUsage: \nPass the url to the chapter as an argument.\nOtherwise call the program with no arguments to have an interactive text interface.");
            else if ((args[0] == "-v") || (args[0] == "--version"))
                Console.WriteLine("v 1.0.0.15");
            else if (args[0].Contains("mangapark"))
                downloadFromString(args[0]);


        }
        public static void downloadFromString(string chapter_url)
        {
            // Part for mass chapter downloads for the same manga
            List<string> chapters_urls = new List<string>();

            string pattern = @"(http.*?c[0-9\.v\s]+)";
            MatchCollection splitted = Regex.Matches(chapter_url, pattern);
            foreach (Match item in splitted)
            {
                chapters_urls.Add(item.ToString());
            }

            if (chapter_url.Contains(" -to "))
            {
                chapters_urls.Clear();

                string patternChapter = @"c([0-9.]+)";
                Match match = Regex.Match(chapter_url, patternChapter);
                int firstchapter = Convert.ToInt32(Regex.Match(chapter_url, patternChapter).ToString().Replace('c', ' '));
                string[] tmp = chapter_url.Split(new string[] { " -to " }, StringSplitOptions.None);
                int lastchapter = Convert.ToInt32(tmp[1]);

                string patternBase = @"(http.*?c)[0-9]";
                string baselineUrl = Regex.Match(chapter_url, patternBase).ToString();
                baselineUrl = baselineUrl.Substring(0,baselineUrl.Length - 1);

                for (int i = firstchapter; i <= lastchapter; i++)
                {
                    chapters_urls.Add(baselineUrl + i.ToString());
                }
            }

            foreach (string s in chapters_urls)
            {
                if (s.Contains("mangapark"))
                    try
                    {
                        dl_chapter(s);
                    }
                    catch
                    {
                        System.Windows.Forms.MessageBox.Show("Ops, something went wrong, probably the link passed wasn't correct");
                        throw;
                    }
            }
            if (path_to_first_file_dl != "")
                Process.Start(path_to_first_file_dl);
        }

        private static void dl_chapter(string url_of_chapter)
        {
            bool skipped = false;
            string html = "";
            try
            {
                html = new WebClient().DownloadString(url_of_chapter);
            }
            catch
            {
                skipped = true;
            }
            if (!skipped)
            {
                string[] lines_of_content = Parser.get_from_1_whats_between_2_and_3(html, "<section id=\"viewer", "</section>").Split('\n');
                List<string> lines_with_urls = new List<string>();
                foreach (string line in lines_of_content)
                {
                    if (line.Contains("src")) lines_with_urls.Add(line);
                }
                int n = 1;
                n_pages = lines_with_urls.Count;
                string title = Parser.get_from_1_whats_between_2_and_3(html, "<title>", "- R").Trim();
                foreach (string url_line in lines_with_urls)
                {
                    dl_image(Parser.get_from_1_whats_between_2_and_3(url_line, "src=\"", "\""),
                             n,
                             title);
                    n++;
                }
                Console.WriteLine("Chapter downloaded.");
            }
            else
            {
                string errorMessage = string.Format("Skipped. No manga chapter found at {0}", url_of_chapter);
                Console.WriteLine(errorMessage);
            }
        }

        private static void dl_image(string url, int n, string foldername)
        {
            WebClient downloader = new WebClient();
            string dl_specific_folder = dl_folder_path + "\\" + foldername;
            string fullpath = dl_specific_folder + "\\img" + n.ToString() + ".jpg";
            if (!Directory.Exists(dl_specific_folder))
                Directory.CreateDirectory(dl_specific_folder);
            if (!File.Exists(fullpath))
            {
                downloader.DownloadFile(new Uri(url), fullpath);
                if (isPTFFDmodifiable)
                {
                    path_to_first_file_dl = fullpath;
                    isPTFFDmodifiable = false;
                }
                Console.WriteLine("Downloaded {0}/{1} of {2}", n, n_pages, foldername);
            }
            else
                Console.WriteLine("Skipped {0}/{1} of {2}, file already downloaded", n, n_pages, foldername);
        }

    }
}
